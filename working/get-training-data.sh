#This script takes a new batch of training data from the roads/working directory and moves it to the corresponding folders
#in the roads/training directory. It can then be used to train the network.
#There should be sub-dirs training/rgb, training/txt and training/mask for this to work.
#In order to get ground truth masks the user must have used labelme on the corresponding images and saved the
#resulting json files to the roads/working/labelme directory.


#Instructions:

#Run in roads/working directory.

#Make sure all 3 parts of each tile (rgb, json, txt) are present in the appropriate directories
#   or else there will be mismatched data in the training set

#Set id to whatever prefix was used in extract-tiles.sh.


 

id="j"
for file in `ls ./labelme`;
do labelme_json_to_dataset ./labelme/$file 2> /dev/null
done


for i in {0..9};
do cp ./labelme/${id}0000${i}_json/img.png ../training/rgb/${id}0000${i}.png 2> /dev/null
cp ./labelme/${id}0000${i}_json/label.png ../training/mask/${id}0000${i}.png 2> /dev/null
cp ./txt/${id}0000${i}.txt ../training/txt/${id}0000${i}.txt 2> /dev/null
done

for i in {10..90};
do cp ./labelme/${id}000${i}_json/img.png ../training/rgb/${id}000${i}.png 2> /dev/null
cp ./labelme/${id}000${i}_json/label.png ../training/mask/${id}000${i}.png 2> /dev/null
cp ./txt/${id}000${i}.txt ../training/txt/${id}000${i}.txt 2> /dev/null
done
