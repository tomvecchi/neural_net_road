#This script uses a kml polygon file (from Roames World etc) and generates the corresponding RGB and dx/dy tiles. 
#Each tile is 256x256 px, covering an area of 400x400 m.


#Instructions:

#Run this in the appropriate directory. There should be sub-dirs named rgb, txt and tiles respectively.

#Generated tiles are placed in the relevant directories- wkt tiles go to ./tiles, rgb tiles go into ./images, 
#   dx/dy gradient tiles go into ./txt

#Once the masks have been obtained via labelme, run the get-training-data.sh script to copy the tiles into the 
#   training folder for training the neural net.

#Use the parameter variables below to avoid name clashes in training data folders. id is a prefix that 
#   identifies each batch of training images (e.g. those from the same location). filename is the kml to use.




id="f" #location name identifier- prefixed to tile names
filename="fortitude" #file name for KML, WKT files
echo ${filename}

#GeometryUtility2 -kmlfile ${filename}.kml -outputcrs 32656,ausgeoid09 > ${filename}.wkt 
GeometryTiler -geometryfile ${filename}.wkt -tilesize "400" -nocliptiles -outputdir tiles 

cd rgb 
echo "extracting RGB"

#extract RGB
for i in {0..9};
do ExtractRGB2 -repository aws:environment=uat \
-geometryfile "../tiles/0000$i.wkt" \
-assetfilter relational:tag=assetTime,op=GREATER_EQUAL,value=2016-01-01 \
-assetfilter relational:tag=assetTime,op=LESS_EQUAL,value=2019-01-31 \
-imagesize 256 256 -outputfile ${id}0000$i.png -loglevel Progress || echo "Error on $i"
    echo "extracted $i"
done;


#extract DTM

cd ..
echo "extracting DTM"
cd txt


for i in {0..9};
  do ExtractDTMGrid -loglevel Progress -repository aws:environment=uat -geometryfile "../tiles/0000$i.wkt" -gridsize 256 256 \
    -nodata -5.0 -sink "text:file=${id}0000${i}.txt,format=XYG,skipnodata=false";

    done

cd ..

