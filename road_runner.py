#This is an attempt at writing a version of the road segmentator that could be used as part of a larger system.
#The other ones worked well as demonstrations of the neural net, but they're not so useful at actually generating predictions
#for arbitrary images, since the file paths etc are all hard coded, and the data must be extracted beforehand.
#road_runner.py takes a geometry file as a command line arg, extracts the data, and saves a series of predicted tiles. 
#My goal was to have it work similarly to ExtractRGB2, where you just give it the geometry and it does the rest automatically.

#RGB version takes about 30 seconds per tile, similar for 5 layer version
#DTM-only takes about 4 seconds/tile
#Speed bottleneck is getting data from georepo- ExtractRGB2 is very slow

from __future__ import print_function
import os
import sys
import cv2
import util
import time
import keras
import psutil
import models
import skimage
import warnings
import argparse
import subprocess
import numpy as np
import random as rn
from util import get_dxy_from_txt
from keras.models import Model, load_model

#Parameters
NUMBER_OF_CLASSES = util.NUMBER_OF_CHANNELS #Number of mask layers to output
IMAGE_W = util.IMAGE_W
IMAGE_H = util.IMAGE_H

temp_dir = "./AAtmp/"


#Takes a WKT tile, extracts corresponding rgb and dxy files
#Saves results to disk. Could maybe speed this up by somehow loading from 
#geo repo straight into memory???
def extract_tiles(filename, layers, verbosity):
    loglevel = "Error"
    if(verbosity != "Normal"):
        loglevel = verbosity
        print("Extracting data for tile %s" % filename)

    if(layers != "dtm"):
        extract_rgb_args = ("ExtractRGB2 -repository aws:environment=uat -geometryfile %s%s \
            -assetfilter relational:tag=assetTime,op=GREATER_EQUAL,value=2016-01-01 -assetfilter \
            relational:tag=assetTime,op=LESS_EQUAL,value=2019-01-31 \
            -imagesize 256 256 -outputfile %s%s.png -loglevel %s" % \
            (temp_dir, filename, temp_dir, filename.split(".")[0], loglevel))

        try:
            subprocess.check_output(extract_rgb_args, shell=True)
        except subprocess.CalledProcessError as cpe:
            print(cpe.output)

    if(layers != "rgb"):
        extract_txt_args = ("ExtractDTMGrid -loglevel %s -repository aws:environment=uat \
            -geometryfile %s%s -gridsize 256 256 \
            -nodata -5.0 -sink 'text:file=%s%s.txt,format=XYG,skipnodata=false'" \
            % (loglevel, temp_dir, filename, temp_dir, filename.split(".")[0]))

        try:
            subprocess.check_output(extract_txt_args, shell=True)
        except subprocess.CalledProcessError as cpe:
            print(cpe.output)




 #Load various layers of a tile depending on what version we're using
def load_image(filename, layers):
    if(layers == "5layer"):
        try:
            rgb = skimage.data.imread(temp_dir+filename.split(".")[0]+".png")
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                rgb = skimage.transform.resize(rgb, (IMAGE_H, IMAGE_W))
        except:
            print("Error loading RGB5 from %s" % temp_dir+filename.split(".")[0]+".png")
        try:
            dx, dy = util.get_dxy_from_txt(temp_dir+filename.split(".")[0]+".txt")
        except:
            print("Error loading txt5 from %s" % temp_dir+filename.split(".")[0]+".txt" )    
        
        dxy = np.stack((dx, dy), axis=-1)
        img = np.concatenate((rgb, dxy), axis=2)


    elif(layers == 'rgb'):
        try:
            img = skimage.data.imread(temp_dir+filename.split(".")[0]+".png")
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                img = skimage.transform.resize(img, (IMAGE_H, IMAGE_W))
        except:
            print("Error loading RGB for %s" % filename)


    elif(layers == 'dtm'):
        try:
            dx, dy = util.get_dxy_from_txt(temp_dir+filename.split(".")[0]+".txt")
        except:
            print("Error loading txt for %s" %filename)    
        
        img = np.stack((dx, dy), axis=-1)

    return img


#Get 400x400m wkt tiles from KML/WKT file and return list of tiles
def get_wkt_tiles(kmlfile, verbosity): 
    loglevel = "Warning"
    if(verbosity != "Normal"):
        loglevel = "Info"


    if (kmlfile.split(".")[1] == "kml"): #KML to WKT
        geo_util2_args = ("GeometryUtility2 -kmlfile %s -outputcrs 32656,ausgeoid09 > %s.wkt \
            -loglevel %s" % (kmlfile, kmlfile.split(".")[0], loglevel))
        try:
            subprocess.check_output(geo_util2_args, shell=True)
            print("Got wkt file from kml")
        except subprocess.CalledProcessError as cpe:
            print(cpe.output)

    geometry_tiler_args = ("GeometryTiler -geometryfile %s.wkt -tilesize '400' -nocliptiles \
        -loglevel %s -outputdir %s" % (kmlfile.split(".")[0], loglevel, temp_dir))
    try:
        subprocess.check_output(geometry_tiler_args, shell=True)
        print("Got WKT tiles")
    except subprocess.CalledProcessError as cpe:
        print(cpe.output)

    
    return sorted(os.listdir(temp_dir))


#Get rid of RGB and txt files when they are no longer needed
def delete_working_files(filename):  
    path = temp_dir+(filename.split(".")[0])
    os.remove(temp_dir+filename)
    if(os.path.isfile(path+".txt")):
        os.remove(path+".txt")
    if(os.path.isfile(path+".png")):
        os.remove(path+".png")    
    return







#------------------------------------
#          Entry point
#------------------------------------
if __name__ == '__main__': 
    start = time.time()
    endline="\r"

    parser = argparse.ArgumentParser()
    parser.add_argument('-geometry', required=True, type=str, help="KML or WKT file containing query geometry")
    parser.add_argument("-layers", default="5layer", type=str, help="Info to use: rgb, dtm or 5layer")
    parser.add_argument("-outputdir", default="./", type=str, help="Output directory")
    parser.add_argument('--show', action='store_true', help="Show the generated prediction onscreen")
    parser.add_argument('--cleanup', action='store_true', help="Delete working files (rgb, dtm etc) after they are used")
    parser.add_argument("-loglevel", type=str, default='Normal', help='Output verbosity: Normal, Progress or Verbose')
    args = parser.parse_args()
    args = vars(args)

    if(os.path.isdir(temp_dir) == False): #Create a working directory to hold WKT and other data
        os.mkdir(temp_dir)

    gfiles = get_wkt_tiles(args["geometry"], args['loglevel'])  #Extract 400x400 m tiles

    if(args['outputdir'][-1] != '/'): #Correct for common typo
        args["outputdir"] += "/"  

    if(args['loglevel'] != "Normal"):
        print(args)
        print(gfiles)
        endline = "\n"
    
    if(args["layers"] == '5layer'): #Only load model once- this should be faster
                                    #Could replace with S3 download commands
        model = load_model("./past_models/5layer_400x28.h5")
    elif(args["layers"] == 'rgb'):
        model = load_model("./past_models/rgb_500x14.h5")
    elif(args['layers'] == 'dtm'):
        model = load_model("./past_models/dxy_500x28.h5")

    print("Got neural net")


    
    for filename in gfiles: #Get data and prediction for one tile at a time-
                            #This will use less memory and will still give partial results if interrupted midway through
        if(filename.split(".")[1] != "wkt"):
            print("The file %s is not a WKT file" % (temp_dir+filename))
            continue
        if(os.path.isfile(temp_dir+filename) == False):
            print("The file %s does not appear to exist" % (temp_dir+filename))
            continue
    
        extract_tiles(filename, args["layers"], args['loglevel'])
        img = load_image(filename, args['layers'])

        y_pred = model.predict(img[None,...].astype(np.float32))[0]    #Get predicted tile
        y_pred = y_pred.reshape((IMAGE_H,IMAGE_W,NUMBER_OF_CLASSES))
        
        print(("Saving prediction for %s"% filename), end=endline)
        sys.stdout.flush()

        if (args['show']):
            cv2.imshow("y_pred",y_pred)
            cv2.imshow("img",img[:,:,0])
            cv2.waitKey(0)

        if (args["cleanup"]):
            delete_working_files(filename)
            if (args['loglevel'] != "Normal"):
                print("Removing "+filename)

        cv2.imwrite(args['outputdir']+filename.split(".")[0]+"_pred.png", y_pred*255) #Save prediction



    if (args["cleanup"]):
            os.rmdir(temp_dir)

    if (args['loglevel'] != "Normal"):
        finish = time.time() - start
        print("Time elapsed = ", finish)

    print()



    



